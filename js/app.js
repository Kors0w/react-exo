class App extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            comment: "",
            comments: []
        }
    }

    handleClick = (event) => {
        console.log(`Nom: ${this.state.name}`)

        this.state.comments = this.state.comments.concat({
            "Nom" : this.state.name,
            "comment" : this.state.comment
        })

        console.log(this.state.comments)
    }

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    render() {
        return(
            <form className="form">
                <section>
                <label className="form__label" htmlFor="name">Name</label>
                </section>

                <section>
                <input className="form__textArea" type="text" id="name" placeholder={"Your name"} onChange={this.handleChange}/>
                </section>

                <section>
                <textarea rows="5" cols="22" className="form__textArea" id="comment" placeholder={"Your comment"} onChange={this.handleChange}/>
                </section>

                <button className="form__button" type="button" id="button" onClick={this.handleClick}>Comment ➤</button>
            </form>

        );
    }
}

ReactDOM.render(<App/>,document.getElementById("app"));